import ShoppingCart from './shopping_cart.svg'
import Star from './star.svg'
import Cancel from './cancel.svg'
import Warning from './warning.svg'
import Happy from './happy.svg'
import Unhappy from './unhappy.svg'
import Maybe from './maybe.svg'

export default {
  ShoppingCart,
  Star,
  Cancel,
  Warning,
  Happy,
  Unhappy,
  Maybe,
}
