import {Component, Inject, OnInit} from '@angular/core';
import * as moment from 'moment';

import {AppBootstrap} from "./app.bootstrap";
import {OutlookService} from "./infrastructure/services/outlook.service";
import {BestelbewusterService} from "./infrastructure/services/bestelbewuster.service";
import * as OfficeHelpers from '@microsoft/office-js-helpers';
import * as md5 from 'js-md5';
import {OfficejsService} from "./infrastructure/services/officejs.service";
const template = require('./app.component.html');

@Component({
  selector: 'app-home',
  template
})
export default class AppComponent implements OnInit {
  public event: any;
  private collapsed: number;
  public convertedDate;

 public constructor(
      @Inject(AppBootstrap) private bootstrap,
      @Inject(OutlookService) private outlookService,
      @Inject(BestelbewusterService) private bestelBewusterService,
      @Inject(OfficejsService) private officeJsService
  ) {
    this.collapsed = undefined;
     this.bootstrap.run();
  }


  public renderStatus(status, type)
  {
    const statusses = {
      // Event statusses
       REGISTERED: {
        translation: 'besteld',
        icon: './src/taskpane/app/images/icons/shopping_cart.png'
       },
       CREATED: {
        translation: 'besteld',
        icon: './src/taskpane/app/images/icons/shopping_cart.png'
       },
       PROCESSED: {
        translation: 'definitief',
        icon: './src/taskpane/app/images/icons/star.png'
       },
       CANCELLED: {
        translation: 'afgelast',
        icon: './src/taskpane/app/images/icons/cancel.png'
       },
       // Email statusses
       ACCEPTED: {
        translation: 'geaccepteerd',
        icon: './src/taskpane/app/images/icons/happy.png'
       },
       NONE: {
        translation: 'nog niet geantwoord',
        icon: './src/taskpane/app/images/icons/warning.png'
       },
       DECLINED: {
        translation: 'geweigerd',
        icon: './src/taskpane/app/images/icons/unhappy.png'
       },
       TENTATIVE: {
        translation: 'misschien',
        icon: './src/taskpane/app/images/icons/maybe.png'
       },
       CONFIRMED : {
        translation: 'aanwezig',
        icon: './src/taskpane/app/images/icons/happy.png'
       },
       SENT: {
        translation: 'nog niet geantwoord',
        icon: './src/taskpane/app/images/icons/warning.png'
      },
      // Order statuses
      OPEN: {
       translation: 'Geopend',
       icon: './src/taskpane/app/images/icons/happy.png'
      },
      SUCCESS: {
       translation: 'Besteld',
       icon: './src/taskpane/app/images/icons/happy.png'
      }
    }
    return statusses[status][type]
  }

  public isCollapsed(index) {
    return this.collapsed === index;
  }

  public isLoggedIn() {
    return true
  }

  public updateCollapse(index) {
    if (this.collapsed === index) {
      this.collapsed = undefined;
    } else {
      this.collapsed = index;
    }
  }

    public async ngOnInit() {
      let hash = await this.makeHash();

      this.bestelBewusterService.getEventByHash(hash).subscribe((event) => {
          this.event = event;
      });
    }

    public async makeHash()
    {
        let subject = await this.officeJsService.getSubject();
        let location = await this.officeJsService.getLocation();
        let organizer = await this.officeJsService.getOrganizer();
        let start = await this.officeJsService.getStartDate();
        let end = await this.officeJsService.getEndDate();

        let organizerFormat = organizer.emailAddress;
        let startDate = moment(start).utc().format();
        let endDate = moment(end).utc().format();

        return md5(subject + location + organizerFormat + startDate +endDate);
    }


    public login()
    {
        var auth = new OfficeHelpers.Authenticator();
        auth.endpoints.registerMicrosoftAuth("a7a188ad-72a3-4a72-bff5-d91a332e291d");
        auth.authenticate(OfficeHelpers.DefaultEndpoints.Microsoft, true).then((token) => {
            console.log(token)
        });

        Office.context.auth.getAccessTokenAsync({forceConsent: true, forceAddAccount: true}, function (result) {
            console.log(result);
        });
    }
}
