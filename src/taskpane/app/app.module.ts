import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import AppComponent from './app.component';
import { AppBootstrap } from "./app.bootstrap";
import {OutlookService} from "./infrastructure/services/outlook.service";
import {HttpClientModule} from "@angular/common/http";
import {BestelbewusterService} from "./infrastructure/services/bestelbewuster.service";
import {OfficejsService} from "./infrastructure/services/officejs.service";

@NgModule({
  declarations: [
      AppComponent
  ],
  imports: [
      BrowserModule,
      AppBootstrap,
      HttpClientModule
  ],
  bootstrap: [
      AppComponent
  ],
  providers: [
      OutlookService,
      BestelbewusterService,
      OfficejsService
  ]
})
export default class AppModule { }
