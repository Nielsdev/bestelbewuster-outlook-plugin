import {Inject, Injectable, NgModule} from '@angular/core';
import { BootstrapConfig } from "./infrastructure/bootstrap/bootstrap.config";
import {SubscribeListenerBootstrap} from "./infrastructure/bootstrap/subscribeListener.bootstrap";

@NgModule({
    imports: [
        BootstrapConfig
    ],
    providers: [
        SubscribeListenerBootstrap
    ]
})

@Injectable()
export class AppBootstrap {
    constructor(
        @Inject(BootstrapConfig) private bootstrap
    ) {

    }

    public async run() {
        for ( const bootstrappable of this.bootstrap.get() ) {
            await bootstrappable.run();
        }
    }
}

