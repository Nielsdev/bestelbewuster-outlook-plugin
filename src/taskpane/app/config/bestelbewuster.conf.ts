export const config = {
    bestelbewuster: {
        endpoint: 'https://r7051nukg0.execute-api.eu-central-1.amazonaws.com/v1/'
    },
    office: {
        outlook: {
            endpoint: "https://outlook.office.com/api/v2.0/",
            subscription: {
                "@odata.type":"#Microsoft.OutlookServices.PushSubscription",
                "Resource": "https://outlook.office.com/api/v2.0/me/events?$select=Organizer,Location,Start,End,Attendees,Subject", //&$filter=Extensions/any(f:f/Id%20eq%20'nl.bestelbewuster.meeting')
                "NotificationURL": "https://r7051nukg0.execute-api.eu-central-1.amazonaws.com/v1/notifications",
                "ChangeType": "Created, Updated, Deleted",
                "ClientState": "c75831bd-fad3-4191-9a66-280a48528679"
            },
            extension: {
                "@odata.type": "Microsoft.OutlookServices.OpenTypeExtension",
                "ExtensionName": "nl.bestelbewuster.meeting",
                "CompanyName": "Bestelbewuster",
                "InitialReferrer":  "Niels de Vries"
            }
        }
    }
};
