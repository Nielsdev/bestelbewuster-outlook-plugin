import {Inject, Injectable, NgModule} from '@angular/core';
import { Bootstrappable } from "../contracts/bootstrappable";
import { SubscribeListenerBootstrap } from "./subscribeListener.bootstrap";

@NgModule({
    providers: [
        SubscribeListenerBootstrap
    ]
})

@Injectable()
export class BootstrapConfig {

    public constructor(
        @Inject(SubscribeListenerBootstrap) private subscribeListenerBootstrap
    ) {
    }

    public get(): Array<Bootstrappable> {
        const bootstrappables: Array<Bootstrappable> = [
            this.subscribeListenerBootstrap
        ];
        return bootstrappables;
    }
}

