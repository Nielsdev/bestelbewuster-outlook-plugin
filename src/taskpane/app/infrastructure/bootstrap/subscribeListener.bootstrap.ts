import {Bootstrappable} from "../contracts/bootstrappable";
import {Inject, Injectable} from "@angular/core";
import {OutlookService} from "../services/outlook.service";
import {BestelbewusterService} from "../services/bestelbewuster.service";
import {config} from "../../config/bestelbewuster.conf";
import {OfficejsService} from "../services/officejs.service";

@Injectable()
export class SubscribeListenerBootstrap implements Bootstrappable
{
    public constructor(
        @Inject(OutlookService) private outlookService: OutlookService,
        @Inject(BestelbewusterService) private bestelbewusterService: BestelbewusterService,
        @Inject(OfficejsService) private officeJsService: OfficejsService
    ) {

    }

    public async run()
    {
        let organizer = await this.officeJsService.getOrganizer();
        this.bestelbewusterService.getSubscriptions(organizer).subscribe((result) => {
            if (result.length == 0) {
                this.outlookService.createSubscription(config.office.outlook.subscription).subscribe((result) => {
                    this.bestelbewusterService.createSubscription(
                        result.Id,
                        'niels@bestelbewuster.nl',
                        result.SubscriptionExpirationDateTime
                    ).subscribe((result) => {

                    });
                });

                return;
            }

            result.forEach((subscription: any) => {
                this.outlookService.renewSubscription(subscription.code).subscribe((result) => {
                    console.log(result)
                    console.log('refreshed')
                }, () => {
                    console.log('failed refresh')
                });
            });
        });
    }

}
