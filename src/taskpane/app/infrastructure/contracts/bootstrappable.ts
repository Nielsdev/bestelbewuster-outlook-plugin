export interface Bootstrappable {
    run(): void;
}

