import {Inject, Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {config} from "../../config/bestelbewuster.conf";

@Injectable()
export class OutlookService {

    public constructor(@Inject(HttpClient) private http: HttpClient) {}

    // Subscriptions

    public createSubscription(subscriptionData: any): Observable<any>
    {
        return this.sendRequest<any>(
            'post',
            'me/subscriptions',
            { body : subscriptionData }
        );
    }

    public renewSubscription(subscriptionId: string): Observable<any>
    {
        return this.sendRequest<any>(
            'patch',
            `me/subscriptions/${subscriptionId}`,
            { body : {
                "@odata.type":"#Microsoft.OutlookServices.PushSubscription",
                "SubscriptionExpirationDateTime": "2022-05-28T17:17:45.9028722Z"
            }}
        )
    }

    public deleteSubscription(subscriptionId: string): Observable<any>
    {
        return this.sendRequest<any>(
            'delete',
            `me/subscriptions${subscriptionId}`
        );
    }

    // Extensions

    public addEventExtension(eventId: string, extensionData: any): Observable<any>
    {
        return this.sendRequest<any>(
            'post',
            'me/events(\''+ eventId+'\')/extensions',
            { body: extensionData }
        );
    }

    public getEventExtension(eventId: string, extensionId: string): Observable<any>
    {
        return this.sendRequest<any>(
            'get',
            `me/events/('${eventId}')/extensions('${extensionId}')`
        );
    }

    public deleteEventExtension(eventId: string, extensionId: string): Observable<any>
    {
        return this.sendRequest<any>(
            'delete',
            `me/events/('${eventId}')/extensions('${extensionId}')`
        );
    }

    // Boilerplate

    private sendRequest<T>(
      method: string,
      resource: string,
      options: {
          body?: any;
          headers?: HttpHeaders | {
              [header: string]: string | string[];
          };
          observe?: 'body';
          params?: HttpParams | {
              [param: string]: string | string[];
          };
          responseType?: 'json';
          reportProgress?: boolean;
          withCredentials?: boolean;
      } = {}
    ): Observable<T> {
        const url = config.office.outlook.endpoint + resource;

        return this.getAuthorizationToken().flatMap((token) => {

            const headers = new HttpHeaders({
                'Authorization': 'Bearer ' + token
            });

            options.headers = headers;

            return this.http.request<T>(method, url, options);
        });
    }

    public getAuthorizationToken(): Observable<string>
    {
        return Observable.create((observer) => {
            Office.context.mailbox.getCallbackTokenAsync({
                isRest: true
            }, (result) => {
                observer.next(result.value);
                observer.complete();
            });
        });
    }
}
