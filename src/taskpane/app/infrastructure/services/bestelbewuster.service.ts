import {Inject, Injectable} from "@angular/core";
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {config} from "../../config/bestelbewuster.conf";
import {Observable} from "rxjs";
import {OfficejsService} from "./officejs.service";

@Injectable()
export class BestelbewusterService
{
    public constructor(
        @Inject(HttpClient) private http: HttpClient
    ) {}

    public getSubscriptions(organizer): Observable<object[]>
    {
        return this.sendRequest('GET', 'subscriptions?owner='+ organizer.emailAddress + '&active=true');
    }

    public createSubscription(code: string, owner: string, expires_at: string): Observable<any>
    {
        return this.sendRequest('POST', 'subscriptions', { body: {
            code,
            owner,
            expires_at
        }});
    }

    public createEvent(): Observable<object[]>
    {
        return Observable.of([]);
    }

    public getEvent(eventId: number): Observable<object[]>
    {
        return this.sendRequest('GET', 'events/' + eventId);
    }

    public getEventByHash(hash: string): Observable<object[]>
    {
        return this.sendRequest('GET', 'events/hash/' + hash )
    }

    private sendRequest<T>(
        method: string,
        resource: string,
        options: {
            body?: any;
            headers?: HttpHeaders | {
                [header: string]: string | string[];
            };
            observe?: 'body';
            params?: HttpParams | {
                [param: string]: string | string[];
            };
            responseType?: 'json';
            reportProgress?: boolean;
            withCredentials?: boolean;
        } = {}
    ): Observable<T> {
        const url = config.bestelbewuster.endpoint + resource;

        return this.http.request<T>(method, url, options);
    }
}
