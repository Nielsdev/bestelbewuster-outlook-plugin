import {Injectable} from "@angular/core";
import AppointmentCompose = Office.AppointmentCompose;
import EmailAddressDetails = Office.EmailAddressDetails;

@Injectable()
export class OfficejsService {

    private item: AppointmentCompose;

    public constructor() {
        this.item = Office.context.mailbox.item;
    }

    public getSubject(): Promise<string>
    {
        return new Promise((resolve, reject) => {
            this.item.subject.getAsync((result) => {
                resolve(result.value)
            })
        })
    }

    public getLocation(): Promise<string>
    {
        return new Promise((resolve, reject) => {
            this.item.location.getAsync((result) => {
                resolve(result.value)
            })
        })
    }

    public getOrganizer(): Promise<EmailAddressDetails>
    {
        return new Promise((resolve, reject) => {
            this.item.organizer.getAsync((result) => {
                resolve(result.value)
            })
        })
    }

    public getStartDate(): Promise<Date>
    {
        return new Promise((resolve, reject) => {
            this.item.start.getAsync((result) => {
                resolve(result.value)
            })
        })
    }

    public getEndDate(): Promise<Date>
    {
        return new Promise((resolve, reject) => {
            this.item.end.getAsync((result) => {
                resolve(result.value)
            })
        })
    }

    public getRequiredAttendees(): Promise<EmailAddressDetails[]>
    {
        return new Promise((resolve, reject) => {
            this.item.requiredAttendees.getAsync((result) => {
                resolve(result.value)
            })
        })
    }

    public getOptionalAttendees(): Promise<EmailAddressDetails[]>
    {
        return new Promise((resolve, reject) => {
            this.item.requiredAttendees.getAsync((result) => {
                resolve(result.value)
            })
        })
    }
}
